#include<stdio.h>
#define MAX 50

struct details{

 char name[MAX];
 char subject[10];
 int marks;
};

int main(){

 int num;
 int i;

 printf("Enter number of students : ");
 scanf("%d",&num);

 struct details details_of_students[num];

 for(i=0; i<num; i++)
 {
   printf("\nEnter student\'s first name : ");
   scanf("%s",&details_of_students[i].name);
   printf("Enter subject : ");
   scanf("%s",&details_of_students[i].subject);
   printf("Enter marks : ");
   scanf("%d",&details_of_students[i].marks);
 }

 for(i=0; i<num; i++)
 {
   printf("\n\nStudent name : %s\n",details_of_students[i].name);
   printf("      %s : %d\n",details_of_students[i].subject,details_of_students[i].marks);

 }
 return 0;
}
